/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.smallrye.common.constraint.NotNull;
import javax.persistence.Entity;


/**
 *
 * @author asus
 */
@Entity
public class Post extends PanacheEntity {
    @NotNull
    public String title;
    @NotNull
    public String content;
    public String tags;

    public Post() {
    }
    
    

    public Post(String title, String content, String tags) {
        this.title = title;
        this.content = content;
        this.tags = tags;
    }
    
   
}
