
package org.controller;

import io.smallrye.common.constraint.NotNull;
import java.util.List;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.domain.Tag;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

/**
 *
 * @author asus
 */
@Path("/tags")
@Produces(MediaType.APPLICATION_JSON)
public class RestApiTag {
    
    @GET
    public Response getList() {
        
        List<Tag> tag =Tag.findAll().list();
        return Response.ok(tag).build();
    }
    
    @GET
    @Path("{id}")
    public Response getSingle(@PathParam Long id) {
        
        Tag tag = Tag.findById(id);
        return Response.ok(tag).build();
    }
    
    @POST
    @Transactional
    public Response save(@NotNull TagModel model){
        
        Tag tag= new Tag(model.label, model.posts);
        tag.persist();
        
        return Response.ok(tag).build();
    }
    
    @PUT
    @Path("{id}")
    @Transactional
    public Response update(@PathParam Long id,@NotNull TagModel model){
        
        Tag tag= Tag.findById(id);
        
        if(tag==null){
            throw new WebApplicationException("Tag with this Id doesn't exist!",404);
        }
        tag.label = model.label;
        tag.posts = model.posts;
             
        tag.persist();
        
        return Response.ok(tag).build();
    }
    
    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam Long id,@NotNull TagModel model){
        
        Tag tag= Tag.findById(id);
        
        if(tag==null){
            throw new WebApplicationException("Tag with this Id doesn't exist!",404);
        }
        tag.delete();
        
        return Response.ok("Delete succesfully!").build();
    }
    
}
