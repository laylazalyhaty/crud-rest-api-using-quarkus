package org.controller;

import io.smallrye.common.constraint.NotNull;
//import io.vertx.mutiny.pgclient.PgPool;
import java.util.List;
//import javax.annotation.PostConstruct;
//import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import org.domain.Post;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.annotations.jaxrs.PathParam;


@Path("/post")
@Produces(MediaType.APPLICATION_JSON)
public class RestApi {

    @GET
    public Response getList() {
        
        List<Post> post =Post.findAll().list();
        return Response.ok(post).build();
    }
    
    @GET
    @Path("{id}")
    public Response getSingle(@PathParam Long id) {
        
        Post post = Post.findById(id);
        return Response.ok(post).build();
    }
    
    @POST
    @Transactional
    public Response save(@NotNull PostModel model){
        
        Post post= new Post(model.title, model.content, model.tags);
        post.persist();
        
        return Response.ok(post).build();
    }
    
    @PUT
    @Path("{id}")
    @Transactional
    public Response update(@PathParam Long id,@NotNull PostModel model){
        
        Post post= Post.findById(id);
        
        if(post==null){
            throw new WebApplicationException("Post with this Id doesn't exist!",404);
        }
        post.title = model.title;
        post.content = model.content;
        post.tags = model.tags;        
        post.persist();
        
        return Response.ok(post).build();
    }
    
    @DELETE
    @Path("{id}")
    @Transactional
    public Response delete(@PathParam Long id,@NotNull PostModel model){
        
        Post post= Post.findById(id);
        
        if(post==null){
            throw new WebApplicationException("Post with this Id doesn't exist!",404);
        }
        post.delete();
        
        return Response.ok("Delete succesfully!").build();
    }
    
//    @Inject
//    PgPool client;
//    
//    @PostConstruct
//    void config(){
//        initdb();
    }
//     private void initdb(){
//        client.query("DROP TABLE IF EXIST post").execute()
//                .flatmap(m->client.query("CREATE TABLE post (id_post INTEGER PRIMARY KEY NOT NULL,"+"title VARCHAR NOT NULL,"+"content VARCHAR NOT NULL,"+"tags VARCHAR)").execute())
//                .flatmap(m->client.query("INSERT INTO post VALUES ('Bla','Lorem ipsum.','Bali')").execute())
//                .flatmap(m->client.query("INSERT INTO post VALUES ('Bli','Lirim ipsim.','Holiday')").execute())
//                .await()
//                .indefinitely();
//    
//          client.query("DROP TABLE IF EXIST tag").execute()
//                .flatmap(m->client.query("CREATE TABLE tag (id_tag INTEGER PRIMARY KEY NOT NULL,"+"label VARCHAR NOT NULL,"+"posts VARCHAR NOT NULL)").execute())
//                .flatmap(m->client.query("INSERT INTO post VALUES ('Lorem ipsum.','Bali')").execute())
//                .flatmap(m->client.query("INSERT INTO post VALUES ('Lirim ipsim.','Holiday')").execute())
//                .await()
//                .indefinitely();

    
